aks_vnet_name = "aksvnet"

sshkvsecret = "keyssh-projetc"

clientidkvsecret = "spn-id"

spnkvsecret = "spn-secret"

vnetcidr = ["10.0.0.0/24"]

subnetcidr = ["10.0.0.0/25"]

keyvault_rg = "projetc-kube-rg"

keyvault_name = "keyvault-projetc"

azure_region = "francecentral"

resource_group = "projetc-rg"

cluster_name = "projetc-cluster"

dns_name = "projetc"

admin_username = "ramydevops"

kubernetes_version = "1.21.7"

agent_pools = {
      name            = "pool1"
      count           = 2
      vm_size         = "Standard_D2_v2"
      os_disk_size_gb = "30"
    }