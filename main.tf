# resource "azurerm_resource_group" "rg-projetc"{
#     name = "rg-projetc"
#     location = "francecentral"

# }
# resource "azurerm_user_assigned_identity" "terraform-identity-projetc" {
#   name                = "terraform-identity-projetc"
#   location            = "francecentral"
#   resource_group_name = "rg-projetc"
# }

resource "azurerm_virtual_network" "aks_vnet" {
  name                = var.aks_vnet_name
  resource_group_name = azurerm_resource_group.aks_rg.name
  location            = azurerm_resource_group.aks_rg.location
  address_space       = var.vnetcidr
} 

resource "azurerm_resource_group" "aks_rg" {
  name     = var.resource_group
  location = var.azure_region
}

